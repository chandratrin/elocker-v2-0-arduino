int data=0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(A4, OUTPUT);
  pinMode(A5, OUTPUT);
  pinMode(A6, OUTPUT);
  pinMode(A7, OUTPUT);
  digitalWrite(4, LOW);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
  digitalWrite(7, LOW);
  digitalWrite(8, LOW);
  digitalWrite(9, LOW);
  digitalWrite(10, LOW);
  digitalWrite(11, LOW);
  digitalWrite(12, LOW);
  digitalWrite(A0, LOW);
  digitalWrite(A1, LOW);
  digitalWrite(A2, LOW);
  digitalWrite(A3, LOW);
  digitalWrite(A4, LOW);
  digitalWrite(A5, LOW);
  digitalWrite(A6, LOW);
  digitalWrite(A7, LOW);
}

void locker(int nilai){
  switch(nilai){
      case 0:
        
        digitalWrite(4, LOW);
        digitalWrite(5, LOW);
        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        digitalWrite(12, LOW);
        digitalWrite(A0, LOW);
        digitalWrite(A1, LOW);
        digitalWrite(A2, LOW);
        digitalWrite(A3, LOW);
        digitalWrite(A4, LOW);
        digitalWrite(A5, LOW);
        digitalWrite(A6, LOW);
        digitalWrite(A7, LOW);  
        break;
      case 1:
        digitalWrite(8, HIGH);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        delay(500);
        break;
      case 2:
        digitalWrite(8, LOW);
        digitalWrite(9, HIGH);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        delay(500);
        break;
      case 3:
        digitalWrite(8, HIGH);
        digitalWrite(9, HIGH);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        delay(500);
        break;
      case 4:
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, HIGH);
        digitalWrite(11, LOW);
        delay(500);
        break;
      case 5:
        digitalWrite(8, HIGH);
        digitalWrite(9, LOW);
        digitalWrite(10, HIGH);
        digitalWrite(11, LOW);
        delay(500);
        break;
      case 6:
        digitalWrite(8, LOW);
        digitalWrite(9, HIGH);
        digitalWrite(10, HIGH);
        digitalWrite(11, LOW);
        delay(500);
        break;
      case 7:
        digitalWrite(8, HIGH);
        digitalWrite(9, HIGH);
        digitalWrite(10, HIGH);
        digitalWrite(11, LOW);
        delay(500);
        break;
      case 8:
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, HIGH);
        delay(500);
        break;
      case 9:
        digitalWrite(8, HIGH);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, HIGH);
        delay(500);
        break;
      case 10:
        digitalWrite(8, LOW);
        digitalWrite(9, HIGH);
        digitalWrite(10, LOW);
        digitalWrite(11, HIGH);
        delay(500);
        break;
      case 11:
        digitalWrite(8, HIGH);
        digitalWrite(9, HIGH);
        digitalWrite(10, LOW);
        digitalWrite(11, HIGH);
        delay(500);
        break;
      case 12:
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, HIGH);
        digitalWrite(11, HIGH);
        delay(500);
        break;
      case 13:
        digitalWrite(8, HIGH);
        digitalWrite(9, LOW);
        digitalWrite(10, HIGH);
        digitalWrite(11, HIGH);
        delay(500);
        break;
      case 14:
        digitalWrite(8, LOW);
        digitalWrite(9, HIGH);
        digitalWrite(10, HIGH);
        digitalWrite(11, HIGH);
        delay(500);
        break;
      case 15:
        digitalWrite(8, HIGH);
        digitalWrite(9, HIGH);
        digitalWrite(10, HIGH);
        digitalWrite(11, HIGH);
        delay(500);
        break;
      case 16:
        digitalWrite(4, HIGH);
        digitalWrite(5, LOW);
        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        delay(500);
        break;
      case 17:
        digitalWrite(4, LOW);
        digitalWrite(5, HIGH);
        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        delay(500);
        break;
      case 18:
        digitalWrite(4, HIGH);
        digitalWrite(5, HIGH);
        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        delay(500);
        break;
      case 19:
        digitalWrite(4, LOW);
        digitalWrite(5, LOW);
        digitalWrite(6, HIGH);
        digitalWrite(7, LOW);
        delay(500);
        break;
      case 20:
        digitalWrite(4, HIGH);
        digitalWrite(5, LOW);
        digitalWrite(6, HIGH);
        digitalWrite(7, LOW);
        delay(500);
        break;
      case 21:
        digitalWrite(4, LOW);
        digitalWrite(5, HIGH);
        digitalWrite(6, HIGH);
        digitalWrite(7, LOW);
        delay(500);
        break;
      case 22:
        digitalWrite(4, HIGH);
        digitalWrite(5, HIGH);
        digitalWrite(6, HIGH);
        digitalWrite(7, LOW);
        delay(500);
        break;
      case 23:
        digitalWrite(4, LOW);
        digitalWrite(5, LOW);
        digitalWrite(6, LOW);
        digitalWrite(7, HIGH);
        delay(500);
        break;
      case 24:
        digitalWrite(4, HIGH);
        digitalWrite(5, LOW);
        digitalWrite(6, LOW);
        digitalWrite(7, HIGH);
        delay(500);
        break;
      case 25:
        digitalWrite(4, LOW);
        digitalWrite(5, HIGH);
        digitalWrite(6, LOW);
        digitalWrite(7, HIGH);
        delay(500);
        break;
      case 26:
        digitalWrite(4, HIGH);
        digitalWrite(5, HIGH);
        digitalWrite(6, LOW);
        digitalWrite(7, HIGH);
        delay(500);
        break;
      case 27:
        digitalWrite(4, LOW);
        digitalWrite(5, LOW);
        digitalWrite(6, HIGH);
        digitalWrite(7, HIGH);
        delay(500);
        break;
      case 28:
        digitalWrite(4, HIGH);
        digitalWrite(5, LOW);
        digitalWrite(6, HIGH);
        digitalWrite(7, HIGH);
        delay(500);
        break;
      case 29:
        digitalWrite(4, LOW);
        digitalWrite(5, HIGH);
        digitalWrite(6, HIGH);
        digitalWrite(7, HIGH);
        delay(500);
        break;
      case 30:
        digitalWrite(4, HIGH);
        digitalWrite(5, HIGH);
        digitalWrite(6, HIGH);
        digitalWrite(7, HIGH);
        delay(500);
        break;
      case 31:
        digitalWrite(A4, HIGH);
        digitalWrite(A5, LOW);
        digitalWrite(A6, LOW);
        digitalWrite(A7, LOW);
        delay(500);
        break;
      case 32:
        digitalWrite(A4, LOW);
        digitalWrite(A5, HIGH);
        digitalWrite(A6, LOW);
        digitalWrite(A7, LOW);
        delay(500);
        break;
      case 33:
        digitalWrite(A4, HIGH);
        digitalWrite(A5, HIGH);
        digitalWrite(A6, LOW);
        digitalWrite(A7, LOW);
        delay(500);
        break;
      case 34:
        digitalWrite(A4, LOW);
        digitalWrite(A5, LOW);
        digitalWrite(A6, HIGH);
        digitalWrite(A7, LOW);
        delay(500);
        break;
      case 35:
        digitalWrite(A4, HIGH);
        digitalWrite(A5, LOW);
        digitalWrite(A6, HIGH);
        digitalWrite(A7, LOW);
        delay(500);
        break;
      case 36:
        digitalWrite(A4, LOW);
        digitalWrite(A5, HIGH);
        digitalWrite(A6, HIGH);
        digitalWrite(A7, LOW);
        delay(500);
        break;
      case 37:
        digitalWrite(A4, HIGH);
        digitalWrite(A5, HIGH);
        digitalWrite(A6, HIGH);
        digitalWrite(A7, LOW);
        delay(500);
        break;
      case 38:
        digitalWrite(A4, LOW);
        digitalWrite(A5, LOW);
        digitalWrite(A6, LOW);
        digitalWrite(A7, HIGH);
        delay(500);
        break;
      case 39:
        digitalWrite(A4, HIGH);
        digitalWrite(A5, LOW);
        digitalWrite(A6, LOW);
        digitalWrite(A7, HIGH);
        delay(500);
        break;
      case 40:
        digitalWrite(A4, LOW);
        digitalWrite(A5, HIGH);
        digitalWrite(A6, LOW);
        digitalWrite(A7, HIGH);
        delay(500);
        break;
      case 41:
        digitalWrite(A4, HIGH);
        digitalWrite(A5, HIGH);
        digitalWrite(A6, LOW);
        digitalWrite(A7, HIGH);
        delay(500);
        break;
      case 42:
        digitalWrite(A4, LOW);
        digitalWrite(A5, LOW);
        digitalWrite(A6, HIGH);
        digitalWrite(A7, HIGH);
        delay(500);
        break;
      case 43:
        digitalWrite(A4, HIGH);
        digitalWrite(A5, LOW);
        digitalWrite(A6, HIGH);
        digitalWrite(A7, HIGH);
        delay(500);
        break;
      case 44:
        digitalWrite(A4, LOW);
        digitalWrite(A5, HIGH);
        digitalWrite(A6, HIGH);
        digitalWrite(A7, HIGH);
        delay(500);
        break;
      case 45:
        digitalWrite(A4, HIGH);
        digitalWrite(A5, HIGH);
        digitalWrite(A6, HIGH);
        digitalWrite(A7, HIGH);
        delay(500);
        break;
      case 46:
        digitalWrite(A0, HIGH);
        digitalWrite(A1, LOW);
        digitalWrite(A2, LOW);
        digitalWrite(A3, LOW);
        delay(500);
        break;
      case 47:
        digitalWrite(A0, LOW);
        digitalWrite(A1, HIGH);
        digitalWrite(A2, LOW);
        digitalWrite(A3, LOW);
        delay(500);
        break;
      case 48:
        digitalWrite(A0, HIGH);
        digitalWrite(A1, HIGH);
        digitalWrite(A2, LOW);
        digitalWrite(A3, LOW);
        delay(500);
        break;
      case 49:
        digitalWrite(A0, LOW);
        digitalWrite(A1, LOW);
        digitalWrite(A2, HIGH);
        digitalWrite(A3, LOW);
        delay(500);
        break;
      case 50:
        digitalWrite(A0, HIGH);
        digitalWrite(A1, LOW);
        digitalWrite(A2, HIGH);
        digitalWrite(A3, LOW);
        delay(500);
        break;
      case 51:
        digitalWrite(A0, LOW);
        digitalWrite(A1, HIGH);
        digitalWrite(A2, HIGH);
        digitalWrite(A3, LOW);
        delay(500);
        break;
      case 52:
        digitalWrite(A0, HIGH);
        digitalWrite(A1, HIGH);
        digitalWrite(A2, HIGH);
        digitalWrite(A3, LOW);
        delay(500);
        break;
      case 53:
        digitalWrite(A0, LOW);
        digitalWrite(A1, LOW);
        digitalWrite(A2, LOW);
        digitalWrite(A3, HIGH);
        delay(500);
        break;
      case 54:
        digitalWrite(A0, HIGH);
        digitalWrite(A1, LOW);
        digitalWrite(A2, LOW);
        digitalWrite(A3, HIGH);
        delay(500);
        break;
      case 55:
        digitalWrite(A0, LOW);
        digitalWrite(A1, HIGH);
        digitalWrite(A2, LOW);
        digitalWrite(A3, HIGH);
        delay(500);
        break;
      case 56:
        digitalWrite(A0, HIGH);
        digitalWrite(A1, HIGH);
        digitalWrite(A2, LOW);
        digitalWrite(A3, HIGH);
        delay(500);
        break;
      case 57:
        digitalWrite(A0, LOW);
        digitalWrite(A1, LOW);
        digitalWrite(A2, HIGH);
        digitalWrite(A3, HIGH);
        delay(500);
        break;
      case 58:
        digitalWrite(A0, HIGH);
        digitalWrite(A1, LOW);
        digitalWrite(A2, HIGH);
        digitalWrite(A3, HIGH);
        delay(500);
        break;
      case 59:
        digitalWrite(A0, LOW);
        digitalWrite(A1, HIGH);
        digitalWrite(A2, HIGH);
        digitalWrite(A3, HIGH);
        delay(500);
        break;
      case 60:
        digitalWrite(A0, HIGH);
        digitalWrite(A1, HIGH);
        digitalWrite(A2, HIGH);
        digitalWrite(A3, HIGH);
        delay(500);
        break;
      default:
   
        digitalWrite(4, LOW);
        digitalWrite(5, LOW);
        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        digitalWrite(12, LOW);
        digitalWrite(A0, LOW);
        digitalWrite(A1, LOW);
        digitalWrite(A2, LOW);
        digitalWrite(A3, LOW);
        digitalWrite(A4, LOW);
        digitalWrite(A5, LOW);
        digitalWrite(A6, LOW);
        digitalWrite(A7, LOW);
      
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()>0){
    int isi = Serial.read();
    locker(isi);
  }
  
}
